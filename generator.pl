#!/usr/bin/perl

use strict;
use warnings;

use Mojo::DOM;
use Data::Dumper;

my $input_xml = do { local $/; <> };

#print $input_xml;

my 	$dom= Mojo::DOM->new->xml(1)->parse($input_xml);
my %paths;
foreach my $i ('a'..'z') {
	$paths{$i}=$dom->at("g#$i path")->{d};
}

my %markov;
my $prev;
foreach my $path (values %paths){
	#print "before:$path\n";
	#$path=~s/\s//g;
	#print "after:$path\n\n\n\n\n";
	my @nodes=split(/([a-z])/i,$path);
	print STDERR Dumper(@nodes);
	shift(@nodes);
	$prev="begin";
	foreach my $node (@nodes){
		$markov{$prev}//=[];
		push(@{$markov{$prev}},$node);
		$prev=$node;
	}
	push(@{$markov{$prev}},"end");
	
}

sub getrand{
	return $_[ rand @_]; #thanks to https://codereview.stackexchange.com/questions/84941/markov-chain-based-random-text-generator-in-perl
}

my $node;
my $next;
print "<svg  viewBox=\"349.569 32.006 25.842 39.965\">";
foreach my $i ('a'..'z'){
	print "<g id=\"$i\">\n";
	print "<path d=\"";
	$node="begin";
	do {
		$next=getrand(@{$markov{$node}});
	} until($next ne "end" or !defined($next));
#	print STDERR Dumper($next);
	while($next ne "end"){
#		print STDERR Dumper($markov{$node});
		print STDERR $next;
		print $next;
		$node=$next;
		$next=getrand(@{$markov{$node}});
	}
	print "\"/>\n";
	print "</g>";
}
print "</svg>";
